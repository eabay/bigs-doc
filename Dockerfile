FROM debian:jessie

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y python-pip python-dev && \
    pip install sphinx sphinx-rtd-theme sphinx-autobuild sphinxcontrib-httpdomain && \
    apt install -y texlive texlive-latex-recommended texlive-latex-extra texlive-fonts-recommended texlive-lang* && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

VOLUME /docs
WORKDIR /docs

CMD bash
