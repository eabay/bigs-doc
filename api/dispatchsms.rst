.. _api-dispatchsms:

DispatchSMS
===========

SMS mesajlarını karşılamak için :ref:`service-application` tarafından sağlanan :download:`servistir </assets/dispatchsms.xml>`.

.. http:post:: /sms-gateway/DispatchSMS

  İstek [#path]_

    .. sourcecode:: http

      POST /sms-gateway/DispatchSMS HTTP/1.1
      Host: 10.x.x.x:5000
      Content-Type: text/xml; charset=utf-8

      <?xml version="1.0" encoding="UTF-8"?>
      <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soapenv:Body>
          <tns:dispatchSMS xmlns:tns="http://bigs.turkcell.com.tr/sms-gateway" soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
            <String_1 xsi:type="xsd:string">&lt;PUSHSERVICE&gt;&lt;VERSION&gt;1.0&lt;/VERSION&gt;&lt;CSPS_SERVICE_ID&gt;6006887&lt;/CSPS_SERVICE_ID&gt;&lt;ROUTE_OBJECT_LIST&gt;&lt;SMS&gt;&lt;MSG_ID&gt;1200685073387120089&lt;/MSG_ID&gt;&lt;MSG_DATE&gt;140315104512&lt;/MSG_DATE&gt;&lt;SRC_MSISDN&gt;05300000000&lt;/SRC_MSISDN&gt;&lt;DST_MSISDN&gt;4549&lt;/DST_MSISDN&gt;&lt;CONTENT&gt;&lt;![CDATA[KRTSN GIRIS TEST]]&gt;&lt;/CONTENT&gt;&lt;MSG_CODE&gt;4549&lt;/MSG_CODE&gt;&lt;XSER&gt;240101&lt;/XSER&gt;&lt;/SMS&gt;&lt;/ROUTE_OBJECT_LIST&gt;&lt;/PUSHSERVICE&gt;</String_1>
          </tns:dispatchSMS>
        </soapenv:Body>
      </soapenv:Envelope>

    ``String_1`` değeri de aslında XML bir ifadedir. Okunabilir hali aşağıdaki gibidir:

    .. sourcecode:: xml

      <PUSHSERVICE>
        <VERSION>1.0</VERSION>
        <CSPS_SERVICE_ID>6006887</CSPS_SERVICE_ID>
        <ROUTE_OBJECT_LIST>
          <SMS>
            <MSG_ID>1200685073387120089</MSG_ID>
            <MSG_DATE>140315104512</MSG_DATE>
            <SRC_MSISDN>05300000000</SRC_MSISDN>
            <DST_MSISDN>4549</DST_MSISDN>
            <CONTENT><![CDATA[KRTSN GIRIS TEST]]></CONTENT>
            <MSG_CODE>4549</MSG_CODE>
            <XSER>000000</XSER>
          </SMS>
        </ROUTE_OBJECT_LIST>
      </PUSHSERVICE>

  Cevap (Başarılı)

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: text/xml; charset=utf-8
    
      <?xml version="1.0" encoding="utf-8"?>
      <env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <env:Body env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
              <m:dispatchSMSResponse xmlns:m="http://bigs.turkcell.com.tr">
                  <result xsi:type="xsd:string">OK</result>
              </m:dispatchSMSResponse>
          </env:Body>
      </env:Envelope>

  Cevap (Başarısız)

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: text/xml; charset=utf-8
    
      <?xml version="1.0" encoding="utf-8"?>
      <env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
          <env:Body env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
              <m:dispatchSMSResponse xmlns:m="http://bigs.turkcell.com.tr">
                  <result xsi:type="xsd:string">NOK, Hata mesajı</result>
              </m:dispatchSMSResponse>
          </env:Body>
      </env:Envelope>

.. [#path] Gönderim yapılan *URL path* :envvar:`DISPATCHSMS_ROUTE` ile düzenlenebilir.