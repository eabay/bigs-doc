SubmitAlarm
===========

TeMIP tarafından :ref:`alarm gönderimi <service-temip-notifier>` için sağlanan :download:`servistir </assets/submitalarm.xml>`.


.. http:post:: /submitalarm/SubmitAlarmService/

  İstek

    .. sourcecode:: http

      POST /submitalarm/SubmitAlarmService/ HTTP/1.1
      Host: 10.x.x.x:9999
      User-Agent: BİGS TeMIP Alarm Submission Service/0.2.2
      Content-Type: text/xml; charset=utf-8
      X-Request-Id: a5325d07-37ca-440e-a0fd-817431848068

      <?xml version="1.0" encoding="UTF-8"?>
      <env:Envelope xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="http://hp.com/temip/openmediation/submitalarm" xmlns:typens="http://hp.com/temip/openmediation/submitalarm/types">
        <env:Body>
          <typens:submitAlarm>
            <alarm>
              <originatingManagedEntity>BTS:ISTATATRKOLMPYL</originatingManagedEntity>
              <alarmType>QUALITY_OF_SERVICE_ALARM</alarmType>
              <probableCause>100</probableCause>
              <perceivedSeverity>CLEAR</perceivedSeverity>
              <specificProblem>1004</specificProblem>
              <alarmRaisedTime>2015-03-16T13:08:11.098Z</alarmRaisedTime>
              <additionalText>EXTERNAL_MOTION
      Source:BIGS
                      </additionalText>
            </alarm>
          </typens:submitAlarm>
        </env:Body>
      </env:Envelope>

  :<header User-Agent: :ref:`service-temip-notifier` servisinin adı ve versiyonu
  :<header X-Request-Id: Sorguya özel UUID
  
  Alanlar
  
    .. csv-table::
      :stub-columns: 1
      :delim: |

      originatingManagedEntity | BTS:``SITE EMG NAME``
      alarmType | QUALITY_OF_SERVICE_ALARM
      probableCause | 100
      perceivedSeverity | ``CRITICAL - Alarm oluştu`` ``CLEAR - Alarm ortadan kalktı``
      specificProblem | TeMIP tarafında :ref:`alarm <alarms>` için ayırt edici olarak kullanılan :ref:`değer <specific-problems>`.
      alarmRaisedTime | Alarmın oluştuğu zaman
      additionalText | :ref:`Alarm <alarms>`, varsa alarma bağlı ek değerler ve son olarak da ifadesi TeMIP tarafından alarmın BİGS tarafından gönderildiğinin tespiti için kullanılan ``Source:BIGS`` satırları şeklinde bir yapıya sahiptir.


  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: text/xml; charset=utf-8
    
      <?xml version="1.0"?>
      <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <soap:Body>
          <ns2:submitAlarmResponse xmlns:ns2="http://hp.com/temip/openmediation/submitalarm/types"/>
        </soap:Body>
      </soap:Envelope>
      


.. _specific-problems:

specificProblem Değerleri
-------------------------

.. csv-table::
  :header-rows: 1
  :delim: :

  Alarm: specificProblem
  HEARTBEAT           : 0
  DEVICE_UNREACHABLE  : 1000
  DEVICE_BOX_OPEN     : 1001
  UNAUTHORIZED_ACCESS : 1002
  INTERNAL_MOTION     : 1003
  EXTERNAL_MOTION     : 1004
  FLOOD               : 1005
  FIRE                : 1006
  SHOCK               : 1007
  ACCESS_GRANTED      : 1008
  REPORT_DELIVERY_PROBLEM : 1009
  SHORT_CIRCUIT_INTERNAL_PIR : 1102
  SHORT_CIRCUIT_EXTERNAL_PIR : 1103
  SHORT_CIRCUIT_WATER_SENSOR : 1104
  SHORT_CIRCUIT_FIRE_SENSOR  : 1105
  SHORT_CIRCUIT_SHOCK_SENSOR : 1106
  CABLE_CUT_INTERNAL_PIR : 1200
  CABLE_CUT_EXTERNAL_PIR : 1201
  CABLE_CUT_WATER_SENSOR : 1202
  CABLE_CUT_FIRE_SENSOR  : 1203
  CABLE_CUT_SHOCK_SENSOR : 1204