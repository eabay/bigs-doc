.. _api-device-sms:

Cihaz SMS API
==============

:ref:`device` HTTP API yanında SMS ile kapı kilidi açma-kapama ve cihazı yeniden başlatma için basit bir API sağlamaktadır.


.. contents:: Başlıklar
  :local:


Kapı Kilidi
-----------
Kapı kilidi aç-kapa istekleri

:Aç: ``RIGHT``
:Kapat: ``LEFT``

Cihaz RESET
-----------

:Yeniden Başlat: ``RESET_BIGS``