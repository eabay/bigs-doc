.. _api-device-gateway:

Cihaz Ağ Geçidi API
===================

:ref:`Cihazlar <device>` tarafından gönderilen mesajlar için :ref:`service-device-gateway` tarafından sağlanan API'dır.


.. contents:: Başlıklar
  :local:


Genel Sorgu
-----------

.. http:post:: /v1

  Cihaz JSON formatında bir yada birden fazla mesaj gönderebilir.
  
  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json
    X-Forwarded-For: 10.x.x.x
    X-Real-IP: 10.x.x.x

    {
      "<message_type>": {}
    }
    
  ya da
  
  .. sourcecode:: json
    
    [{
      "<message_type>": {}
    }]

  :<header Content-Type: ``application/json`` Sorgu içeriği JSON formatındadır.
  :<header X-Forwarded-For: Sorguyu direkt olarak servisin karşılamadığı durumlarda cihazın IP adresini tespit etmek için kullanılır.
  :<header X-Real-IP: *X-Forwarded-For* bilgisinin bulunmaması durumunda IP adresi buradan okunmaya çalışılır.
  
  .. note:: ``X-Forwarded-For`` veya ``X-Real-IP`` başlıklarından ikisinin de bulunmaması durumunda bağlantının yapıldığı IP adresi cihaz IP adresi olarak kullanılır.
  
  :status 204: Mesaj(lar) başarıyla kuyruğa alındı.
  :status 500: Mesaj(lar)ın bazıları kuyruğa alındı.
  :status 503: Mesajar(lar) kuyruğa alınamadı.
  

.. _api-report:

Rapor
-----

JSON Şema: :download:`report.json </assets/schemas/report.json>`

.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "report": {
        "id": "14141410-53320000-524d6084-f5000003",
        "ver": 4,
        "time": "15/02/05,15:24:53",
        "door": 0,
        "box_open": 0,
        "light": 0,
        "comp": {
          "internal_pir": 0,
          "external_pir": 0,
          "water": 0,
          "fire": 0,
          "shock": 0
        },
        "power": {
          "adc1": "0.554297V",
          "adc2": "0.000000V",
          "adc3": "2.760930V"
        }
      }
    }

  :<json string id: Cihaz chip ID
  :<json number ver: Firmware versiyonu
  :<json string time: Raporun oluşturulduğu zaman. Format: ``yy/MM/dd,hh:mm:ss``
  :<json number door: Kapı kilidinin durumu
  :<json number box_open: Kutu kapağı durumu
  :<json number light: Işığın durumu
  :<json object comp: :ref:`Sensörlerin <device-sensors>` durumları
  :<json object power: Saha enerjisi (``adc1``, ``adc2``) ve cihaz pili durumu (``adc3``)


Olay
----

JSON Şema: :download:`event.json </assets/schemas/event.json>`
  
.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "event": {
        "name": "internal_pir_alarm",
        "id": "1313fd10-53570951-4f2dcd97-f5000000",
        "time": "15/02/05,15:26:25",
        "data": {},
        "extra_data": {}
      }
    }

  :<json string name: Olay tipi ``box_opened``, ``box_closed``, ``lights_turned_on``, ``lights_turned_off``, ``internal_pir_alarm``, ``external_pir_alarm``, ``shock_alarm``, ``water_alarm``, ``fire_alarm``
  :<json string id: Cihaz chip ID
  :<json string time: Raporun oluşturulduğu zaman. Format: ``yy/MM/dd,hh:mm:ss``
  :<json object data: Alarma bağlı veri
  :<json object extra_data: Alarma bağlı veri


.. _api-event-alarms:

Alarmlar
********
  
``internal_pir_alarm``, ``external_pir_alarm``, ``shock_alarm``, ``water_alarm``, ``fire_alarm``
  
.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "event": {
        "name": "internal_pir_alarm",
        "id": "1313fd10-53570951-4f2dcd97-f5000000",
        "time": "15/02/05,15:26:25",
        "data": {
          "alarm_type": 1,
        },
        "extra_data": {}
      }
    }
  
  :<json object data.alarm_type: 0-3 arası bir değer alabilir.


.. _api-event-lights_turned_on:

Işık açıldı
***********
  
.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "event": {
        "name": "lights_turned_on",
        "id": "1313fd10-53570951-4f2dcd97-f5000000",
        "time": "15/02/05,15:26:25",
        "data": {},
        "extra_data": {}
      }
    }


.. _api-event-lights_turned_off:

Işık kapandı
************

.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "event": {
        "name": "lights_turned_off",
        "id": "1313fd10-53570951-4f2dcd97-f5000000",
        "time": "15/02/05,15:26:25",
        "data": {},
        "extra_data": {}
      }
    }

.. _api-event-box_opened:
  
Kutu kapağı açıldı
******************

.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "event": {
        "name": "box_opened",
        "id": "1313fd10-53570951-4f2dcd97-f5000000",
        "time": "15/02/05,15:26:25",
        "data": {},
        "extra_data": {}
      }
    }


.. _api-event-box_closed:

Kutu kapağı kapandı
*******************

.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "event": {
        "name": "box_closed",
        "id": "1313fd10-53570951-4f2dcd97-f5000000",
        "time": "15/02/05,15:26:25",
        "data": {
          "connected": ["internal_pir", "door_lock"]
        },
        "extra_data": {}
      }
    }

  :<json object data.connected: Cihaza bağlı olan bileşenler, ``internal_pir``, ``external_pir``, ``shock``, ``water``, ``fire``, ``door_lock``.


.. _api-command_response:

Komut Yanıtı
------------
JSON Şema: :download:`command_response.json </assets/schemas/command_response.json>`

.. http:post:: /v1

  .. sourcecode:: http

    POST /v1 HTTP/1.1
    Host: bigs:3000
    Content-Type: application/json

    {
      "command_response": {
        "id": "505010c-53570953-4f2f48aa-f5000007",
        "cmd_id": "WmSZQ9xXFxIyW0Uu",
        "status": 1,
        "message": "response message"
      }
    }

  :<json string id: Cihaz chip ID
  :<json string cmd_id: Komut ID
  :<json number status: Sonuç ``1 - Başarılı``, ``0 - Başarısız``
  :<json string reason: Başarısızlık nedeni
