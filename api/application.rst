.. _api-application:

Uygulama API
============

:ref:`service-application` tarafından sağlanan API.

.. contents:: Başlıklar
  :local:

Giriş
-----

.. http:post:: /authenticate

  İstek

    .. sourcecode:: http

      POST /authenticate HTTP/1.1
      Host: bigs:5000
      Content-Type: application/x-www-form-urlencoded

      username=user&password=pass
    
  :form username: Kullanıcı adı
  :form password: Şifre

  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json
    
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX"

  :status 200: Cevap olarak Auth Token döner. Bu değer tüm sorgularda :http:header:`Authorization` değeri olarak kullanılır.
  :status 401: Hatalı giriş


Kullanıcı Bilgileri
-------------------

.. http:get:: /me
  
  İstek
  
    .. sourcecode:: http

      GET /me HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json
    
      {
        "name":"Dev User",
        "email":"dev@dev.",
        "username":"dev",
        "is_admin":false
      }
  
  :status 200: Giriş yapan kullanıcıya ait bilgiler döner.
  :status 403: Yetkisiz giriş

  :>json string name: İsim
  :>json string email: E-posta
  :>json string username: Kullanıcı adı
  :>json boolean is_admin: Yönetici haklarına sahip mi?


Sahalar
-------

.. http:get:: /sites
  
  İstek
  
    .. sourcecode:: http

      GET /sites HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json
    
      [
        {
          "_id": "OLMSY",
          "device": {
            "_id": "6061618-53580ea0-4fbd80fb-f5111114",
            "region": "TRAKYA",
            "msisdn": "5300000000",
            "last_message_at": "2015-03-12T09:48:02.539Z",
            "firmware": "5",
            "updated_at": "2015-03-12T09:48:29.414Z",
            "created_at": "2014-03-19T09:25:18.434Z",
            "status": {
              "components": {
                "shock": "1",
                "lights": 0,
                "internal_pir": 0,
                "external_pir": "1",
                "door_lock": 0
              },
              "power": {
                "voltageType": "-48",
                "voltage": 49,
                "charge_ratio": 100
              },
              "battery": {
                "voltage": 4.2,
                "charge_ratio": 100
              }
            },
            "components": [
              "lights",
              "external_pir",
              "door_lock",
              "internal_pir",
              "shock"
            ],
            "ip": {
              "apn": "10.x.x.x.x"
            }
          },
          "emg_name": "ISTATATRKOLMPYL",
          "name": "ATATURK OLIMPIYAT STADI YOLU",
          "region": "TRAKYA",
          "alarms": [
            "SHOCK",
            "EXTERNAL_MOTION"
          ],
          "updated_at": "2015-03-12T09:48:30.030Z",
          "created_at": "2014-06-10T06:06:42.291Z",
          "network": {
            "gateway": "10.x.x.x",
            "ip": {
              "device": "10.x.x.x",
              "external_camera": "10.x.x.x",
              "internal_camera": "10.x.x.x"
            }
          },
          "authorized_access": false,
          "loc": {
            "lat": 41.06361111111111,
            "lng": 28.766416666666668
          }
        },
        {
          "_id": "EDBAD",
          "device": {
            "_id": "1414071a-53320000-524d6359-f5111112",
            "__v": 141,
            "region": "TRAKYA",
            "msisdn": "5300000000",
            "last_message_at": "2015-03-12T09:48:07.068Z",
            "firmware": "5",
            "updated_at": "2015-03-12T09:48:08.101Z",
            "created_at": "2014-03-19T09:25:18.422Z",
            "status": {
              "components": {
                "shock": 0,
                "lights": 0,
                "internal_pir": 0,
                "external_pir": 0,
                "door_lock": 0
              },
              "power": {
                "voltageType": "-48",
                "voltage": 56,
                "charge_ratio": 100
              },
              "battery": {
                "voltage": 4.15,
                "charge_ratio": 96
              }
            },
            "components": [
              "lights",
              "internal_pir",
              "external_pir",
              "shock",
              "door_lock"
            ],
            "ip": {
              "apn": "10.x.x.x"
            }
          },
          "emg_name": "EDIMERKZBADEM",
          "name": "EDIRNE BADEMLIK",
          "region": "TRAKYA",
          "alarms": [],
          "updated_at": "2015-03-12T06:22:06.740Z",
          "created_at": "2014-06-10T06:06:42.259Z",
          "network": {
            "gateway": "10.x.x.x",
            "ip": {
              "device": "10.x.x.x",
              "external_camera": "10.x.x.x",
              "internal_camera": "10.x.x.x"
            }
          },
          "authorized_access": false,
          "loc": {
            "lat": 41.68169444444444,
            "lng": 26.584055555555555
          }
        }
      ]
  
  :status 200: Sahalar
  :status 403: Yetkisiz giriş
  :status 500: İstek başarısız



Saha
----

.. http:get:: /sites/(site_id)
  
  İstek
  
    .. sourcecode:: http

      GET /sites/VELIK HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK
      Content-Type: application/json
    
      {
        "_id":"ADGOK",
        "emg_name":"ADPGOKTEPEKOYU",
        "region":"İZMİT",
        "name":"GOKTEPE",
        "device":{
          "_id":"11110a02-53320000-5267162e-f5111116",
          "region":"İZMİT",
          "last_message_at":"2015-03-12T10:14:10.742Z",
          "msisdn":"5300000000",
          "firmware":"5",
          "updated_at":"2015-03-12T10:14:10.770Z",
          "created_at":"2014-09-04T10:47:23.406Z",
          "status":{
            "components":{
              "shock":0,
              "door_lock":0,
              "external_pir":0,
              "internal_pir":0,
              "lights":0
            },
            "power":{
              "voltageType":"-48",
              "voltage":56,
              "charge_ratio":100
            },
            "battery":{
              "voltage":4.22,
              "charge_ratio":100
            }
          },
          "components":[
            "lights",
            "internal_pir",
            "external_pir",
            "shock",
            "door_lock"
          ],
          "ip":{
            "apn":"10.x.x.x"
          },
          "id":"11110a02-53320000-5267162e-f5111116"
        },
        "alarms":[

        ],
        "updated_at":"2015-03-12T09:48:10.515Z",
        "created_at":"2014-06-16T08:03:53.394Z",
        "network":{
          "gateway":"10.x.x.x",
          "ip":{
            "external_camera":"10.x.x.x",
            "internal_camera":"10.x.x.x",
            "device":"10.x.x.x"
          }
        },
        "authorized_access":false,
        "loc":{
          "lng":30.41169444444444,
          "lat":40.85458333333334
        },
        "id":"ADGOK",
        "activities":[
          {
            "_id":"550160c2c35ef4100049ab5d",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-12T09:48:11.102Z",
              "type":"EXTERNAL_MOTION",
              "raised_at":"2015-03-12T09:47:46.601Z"
            },
            "created":"2015-03-12T09:47:46.601Z"
          },
          {
            "_id":"550160adc35ef4100049ab25",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-12T09:48:10.968Z",
              "type":"SHOCK",
              "raised_at":"2015-03-12T09:47:25.779Z"
            },
            "created":"2015-03-12T09:47:25.779Z"
          },
          {
            "_id":"54f881b279329b930de19de7",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-05T16:18:08.466Z",
              "type":"SHOCK",
              "raised_at":"2015-03-05T16:17:54.726Z"
            },
            "created":"2015-03-05T16:17:54.726Z"
          },
          {
            "_id":"54f333916058434b0860764c",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-01T15:44:01.397Z",
              "type":"EXTERNAL_MOTION",
              "raised_at":"2015-03-01T15:43:13.624Z"
            },
            "created":"2015-03-01T15:43:13.624Z"
          },
          {
            "_id":"54f333806058434b0860764b",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-01T15:43:01.753Z",
              "type":"UNAUTHORIZED_ACCESS",
              "raised_at":"2015-03-01T15:42:56.677Z"
            },
            "created":"2015-03-01T15:42:56.677Z"
          },
          {
            "_id":"54f333486058434b0860764a",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-01T15:43:01.679Z",
              "repeats":[
                "2015-03-01T15:42:04.136Z",
                "2015-03-01T15:42:21.405Z"
              ],
              "type":"SHOCK",
              "raised_at":"2015-03-01T15:42:00.320Z"
            },
            "created":"2015-03-01T15:42:00.320Z"
          },
          {
            "_id":"54f333447fb5cfc807925582",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-01T15:43:01.673Z",
              "repeats":[
                "2015-03-01T15:42:04.131Z"
              ],
              "type":"EXTERNAL_MOTION",
              "raised_at":"2015-03-01T15:41:56.632Z"
            },
            "created":"2015-03-01T15:41:56.632Z"
          },
          {
            "_id":"54f331c78ef7773e0881d715",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-01T15:36:01.455Z",
              "type":"SHOCK",
              "raised_at":"2015-03-01T15:35:35.873Z"
            },
            "created":"2015-03-01T15:35:35.873Z"
          },
          {
            "_id":"54f331b04b1922bb07ea4f89",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-03-01T15:36:01.412Z",
              "repeats":[
                "2015-03-01T15:35:47.043Z"
              ],
              "type":"EXTERNAL_MOTION",
              "raised_at":"2015-03-01T15:35:12.267Z"
            },
            "created":"2015-03-01T15:35:12.267Z"
          },
          {
            "_id":"54ef9f06df48786a0395742b",
            "site":"ADGOK",
            "type":"alarm",
            "data":{
              "cleared_at":"2015-02-26T22:32:56.889Z",
              "type":"SHOCK",
              "raised_at":"2015-02-26T22:32:38.990Z"
            },
            "created":"2015-02-26T22:32:38.990Z"
          },
          {
            "_id":"54cdda0d4c22de1000caae75",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"sms",
              "person":{
                "name":"VOLKAN DEMİRKAYA",
                "phone":"05300000000"
              },
              "reason":"ARIZA "
            },
            "created":"2015-02-01T07:47:25.375Z"
          },
          {
            "_id":"54cdd8064c22de1000caae74",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"sms",
              "person":{
                "name":"VOLKAN DEMİRKAYA",
                "phone":"05300000000"
              },
              "reason":"ARIZA"
            },
            "created":"2015-02-01T07:38:46.635Z"
          },
          {
            "_id":"54bbe60625d83311007c57a5",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"bigs",
              "reason":"30 dakika boyunca hareket algılanmadığı için saha çıkışı otomatik olarak yapıldı."
            },
            "created":"2015-01-18T16:57:42.071Z"
          },
          {
            "_id":"54bbde15b773d410007d9ccd",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-18T16:23:49.644Z"
          },
          {
            "_id":"54b687ceddef6d100029f332",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"bigs",
              "reason":"30 dakika boyunca hareket algılanmadığı için saha çıkışı otomatik olarak yapıldı."
            },
            "created":"2015-01-14T15:14:22.580Z"
          },
          {
            "_id":"54b67e6fddef6d100029f331",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-14T14:34:23.469Z"
          },
          {
            "_id":"54b2bbdc89c0e34403303a41",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-11T18:07:24.817Z"
          },
          {
            "_id":"54b2b8c50d098e6a03ab1513",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-11T17:54:13.263Z"
          },
          {
            "_id":"54b2190c0d098e6a03ab150f",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-11T06:32:44.666Z"
          },
          {
            "_id":"54b2172e89c0e34403303a3e",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-11T06:24:46.440Z"
          },
          {
            "_id":"54b168df89c0e34403303a3b",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"bigs",
              "reason":"30 dakika boyunca hareket algılanmadığı için saha çıkışı otomatik olarak yapıldı."
            },
            "created":"2015-01-10T18:01:03.549Z"
          },
          {
            "_id":"54b15ad589c0e34403303a3a",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-10T17:01:09.636Z"
          },
          {
            "_id":"54b00aad7427226a03442917",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-09T17:06:53.162Z"
          },
          {
            "_id":"54b009797427226a03442916",
            "type":"maintenance-activated",
            "site":"ADGOK",
            "data":{
              "channel":"web",
              "user":{
                "_id":"539fdcd918a5be8b05e58388",
                "name":"ONUR BEKAROGLU",
                "username":"FST00000"
              }
            },
            "created":"2015-01-09T17:01:45.556Z"
          },
          {
            "_id":"54afc1920d098e6a03ab1506",
            "type":"maintenance-deactivated",
            "site":"ADGOK",
            "data":{
              "channel":"sms",
              "person":{
                "name":"ENGİN GÖREN",
                "phone":"05300000000"
              },
              "reason":"ARIZA"
            },
            "created":"2015-01-09T11:54:58.362Z"
          }
        ]
      }
  
  :status 200: Sahalar
  :status 403: Yetkisiz giriş
  :status 500: İstek başarısız


Saha Girişi
-----------

Giriş
*****

.. http:put:: /sites/(site_id)/authorize-access
  
  İstek
  
    .. sourcecode:: http

      PUT /sites/VELIK/authorize-access HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek Başarılı
  :status 400: İstek başarısız
  :status 403: Yetkisiz giriş


Çıkış
*****

.. http:delete:: /sites/(site_id)/authorize-access
  
  İstek
  
    .. sourcecode:: http

      DELETE /sites/VELIK/authorize-access HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek Başarılı
  :status 400: İstek başarısız
  :status 403: Yetkisiz giriş



Kapı Kilidi
-----------

Kilitle
*******

.. http:put:: /sites/(site_id)/lock-door
  
  İstek
  
    .. sourcecode:: http

      PUT /sites/VELIK/lock-door HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Kilidi Aç
*********

.. http:delete:: /sites/(site_id)/lock-door
  
  İstek
  
    .. sourcecode:: http

      DELETE /sites/VELIK/lock-door HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Kilitle (SMS)
*************

.. http:put:: /sites/(site_id)/lock-door-sms
  
  İstek
  
    .. sourcecode:: http

      PUT /sites/VELIK/lock-door-sms HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Kilidi Aç (SMS)
***************

.. http:delete:: /sites/(site_id)/lock-door-sms
  
  İstek
  
    .. sourcecode:: http

      DELETE /sites/VELIK/lock-door-sms HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Işık
----

Aç
**

.. http:put:: /sites/(site_id)/turn-on-lights
  
  İstek
  
    .. sourcecode:: http

      PUT /sites/VELIK/turn-on-lights HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 400: Hatalı istek
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Kapat
*****

.. http:delete:: /sites/(site_id)/turn-on-lights
  
  İstek
  
    .. sourcecode:: http

      DELETE /sites/VELIK/turn-on-lights HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 400: Hatalı istek
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Cihaz RESET
-----------

.. http:put:: /sites/(site_id)/reset-device
  
  İstek
  
    .. sourcecode:: http

      PUT /sites/VELIK/reset-device HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 400: Hatalı istek
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Firmware Güncelleme
-------------------

.. http:put:: /sites/(site_id)/update-device-firmware
  
  İstek
  
    .. sourcecode:: http

      PUT /sites/VELIK/update-device-firmware HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX


  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 No Content
  
  :status 204: İstek başarılı
  :status 400: Hatalı istek
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


Cihaz Kurulumu
--------------

.. http:post:: /provisioning

  İstek

    .. sourcecode:: http

      POST /provisioning HTTP/1.1
      Host: bigs:5000
      Content-Type: application/x-www-form-urlencoded
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX

      device=14141410-53320000-524d6084-f5000003&site=ACIBA
    
  :form device: Cihaz chip ID
  :form site: Saha ID

  Cevap

    .. sourcecode:: http

      HTTP/1.1 204 OK
  
  :status 204: İstek başarılı
  :status 403: Yetkisiz giriş
  :status 500: İstek yerine getirilemedi


SMS Giriş İstekleri
-------------------

.. http:get:: /sms-access

  İstek

    .. sourcecode:: http
    
      GET /sms-access HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX

  Cevap

    .. sourcecode:: http
    
      HTTP/1.1 200 OK
      Content-Type: application/json
  
      [
        {
          "_id":"54e1e79ac4f931100006b224",
          "type":"sms access request",
          "site":{
            "_id":"TEST1",
            "region":"TEST"
          },
          "status":"failed",
          "events":[
            {
              "type":"unauthorized msisdn",
              "data":{
                "status":"failed",
                "msisdn":"05320000001"
              },
              "_id":"54e1e79ac4f931100006b225",
              "time":"2015-02-16T12:50:34.388Z"
            },
            {
              "type":"message validated",
              "data":{
                "status":"completed",
                "raw":"TEST1 GIRIS test",
                "parsed":{
                  "valid":true,
                  "site":"TEST1",
                  "command":"GIRIS",
                  "reason":"test"
                }
              },
              "_id":"54e1e79ac4f931100006b226",
              "time":"2015-02-16T12:50:34.389Z"
            },
            {
              "type":"user sms feedback",
              "data":{
                "to":"05320000001",
                "content":"Kullanıcı doğrulanamadı. Yetkili bir kullanıcı ile giriş yapabilirsiniz. Destek için lütfen 05324444242 numarasını arayınız. İşlem Numarası: TBzzCMVS",
                "status":"completed"
              },
              "_id":"54e1e79ac4f931100006b227",
              "time":"2015-02-16T12:50:34.390Z"
            }
          ],
          "data":{
            "request_id":"TBzzCMVS",
            "sms":{
              "content":"TEST1 GIRIS test",
              "date":"2015-02-06T11:35:01.000Z",
              "id":"1200685037489012334",
              "to":"4549",
              "from":"05320000001"
            }
          },
          "updated_at":"2015-02-16T12:50:34.423Z",
          "created_at":"2015-02-16T12:50:34.423Z"
        },
        {
          "_id":"54d9c93bf0accbf800db3f03",
          "type":"sms access request",
          "site":{
            "_id":"KTLAK",
            "region":"ASYA"
          },
          "status":"completed",
          "events":[
            {
              "type":"msisdn authorized",
              "data":{
                "status":"completed",
                "msisdn":"05320000000",
                "person":"Erhan Abay"
              },
              "_id":"54d9c93cf0accbf800db3f04",
              "time":"2015-02-10T09:02:52.003Z"
            },
            {
              "type":"site access authorized",
              "data":{
                "status":"completed"
              },
              "_id":"54d9c93cf0accbf800db3f06",
              "time":"2015-02-10T09:02:52.025Z"
            },
            {
              "type":"sending unlock command",
              "data":{
                "status":"completed",
                "command":"54d9c93cf0accbf800db3f07",
                "ips":[

                ]
              },
              "_id":"54d9c93cf0accbf800db3f08",
              "time":"2015-02-10T09:02:52.047Z"
            },
            {
              "type":"user sms feedback",
              "data":{
                "to":"05320000000",
                "content":"KTLAK sahasının kapısının açılması için işlem başlatılmıştır. Sonuç bildirilecektir.",
                "status":"completed"
              },
              "_id":"54d9c93cf0accbf800db3f09",
              "time":"2015-02-10T09:02:52.048Z"
            },
            {
              "type":"waiting for door unlock response",
              "data":{
                "status":"completed",
                "command":"54d9c93cf0accbf800db3f07"
              },
              "_id":"54d9c93df0accbf800db3f0b",
              "time":"2015-02-10T09:02:53.770Z"
            },
            {
              "type":"door unlocked",
              "data":{
                "status":"completed"
              },
              "_id":"54d9c95839570a05013d7ec4",
              "time":"2015-02-10T09:03:20.418Z"
            },
            {
              "type":"user sms feedback",
              "data":{
                "to":"05320000000",
                "content":"KTLAK sahasındaki kapı açılmıştır. Kapatmak için KTLAK CIKIS ve işlemi yazarak 4549’a gönderebilirsiniz.",
                "status":"completed"
              },
              "_id":"54d9c95839570a05013d7ec5",
              "time":"2015-02-10T09:03:20.420Z"
            }
          ],
          "data":{
            "request_id":"8kz39bl6",
            "sms":{
              "content":"KTLAK GIRIS test",
              "date":"2015-02-06T11:35:01.000Z",
              "id":"1200685037489012334",
              "to":"4549",
              "from":"05320000000"
            }
          },
          "updated_at":"2015-02-10T09:03:20.422Z",
          "created_at":"2015-02-10T09:02:52.007Z"
        }
      ]

  :query site: Saha ID. Birden fazla saha aralarına virgül konarak gönderilebilir.
  :query msisdn: Telefon numarası
  :query request_id: İşlem numarası


SMS Giriş İsteği Ayarları
-------------------------

.. http:get:: /sms-access/settings

  İstek

    .. sourcecode:: http
    
      GET /sms-access/settings HTTP/1.1
      Host: bigs:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX

  Cevap

    .. sourcecode:: http
    
      HTTP/1.1 200 OK
      Content-Type: application/json
  
      {
        "support_line":"05320000000",
        "templates":{
          "unauthorized_person":"Kullanıcı doğrulanamadı. Yetkili bir kullanıcı ile giriş yapabilirsiniz.",
          "wrong_format":"Girilen mesaj yanlıştır. Giriş yapmak istediğiniz sahanın adını yazıp boşluk bıraktıktan sonra GIRIS ve sonrasında giriş nedeninizi yazarak 4549’a gönderiniz.",
          "site_not_found":"Saha bilgisi hatalı. Destek için lütfen {{ support_line }} numarasını arayınız. İşlem Numarası: {{ request_id }}",
          "access_authorized":"{{ site }} sahasına girişiniz onaylanmıştır.",
          "access_deauthorized":"{{ site }} sahasından çıkışınız onaylanmıştır.",
          "cannot_authorize_access":"{{ site }} sahasına girişiniz onaylanamamıştır. Destek için lütfen {{ support_line }} numarasını arayınız. İşlem Numarası: {{ request_id }}",
          "wait_for_unlock":"{{ site }} sahasının kapısının açılması için işlem başlatılmıştır. Sonuç bildirilecektir.",
          "request_failed":"{{ site }} sahasının kapısı açılamamıştır. Destek için lütfen {{ support_line }} numarasını arayınız.  İşlem Numarası: {{ request_id }}",
          "request_completed":"{{ site }} sahasındaki kapı açılmıştır. Kapatmak için {{ site }} CIKIS ve işlemi yazarak 4549’a gönderebilirsiniz."
        }
      }

  :status 200: İstek başarılı
  :status 401: Yetkisiz giriş
  :status 403: Yönetici hakları gerekli

  :>json string support_line: Destek hattı numarası
  :>json array templates: SMS metinleri için kullanılacak taslaklar



Ayarları Değiştir
*****************

.. http:post:: /sms-access/settings

  İstek

    .. sourcecode:: http
    
      POST /sms-access/settings HTTP/1.1
      Host: bigs:5000
      Content-Type: application/json
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1NGIwMmQ4MDE1YTI1MzI0MDA1NTNmMWIiLCJpYXQiOjE0MjQzMjcwNDcsImV4cCI6MTU5NzEyNzA0N30.wcUOiMjXdFdG9L3cQqOaqDvTdx_g50_PMExowCAnXXX
      
      {
        "support_line":"05320000000",
        "templates":{
          "unauthorized_person":"Kullanıcı doğrulanamadı. Yetkili bir kullanıcı ile giriş yapabilirsiniz.",
          "wrong_format":"Girilen mesaj yanlıştır. Giriş yapmak istediğiniz sahanın adını yazıp boşluk bıraktıktan sonra GIRIS ve sonrasında giriş nedeninizi yazarak 4549’a gönderiniz.",
          "site_not_found":"Saha bilgisi hatalı. Destek için lütfen {{ support_line }} numarasını arayınız. İşlem Numarası: {{ request_id }}",
          "access_authorized":"{{ site }} sahasına girişiniz onaylanmıştır.",
          "access_deauthorized":"{{ site }} sahasından çıkışınız onaylanmıştır.",
          "cannot_authorize_access":"{{ site }} sahasına girişiniz onaylanamamıştır. Destek için lütfen {{ support_line }} numarasını arayınız. İşlem Numarası: {{ request_id }}",
          "wait_for_unlock":"{{ site }} sahasının kapısının açılması için işlem başlatılmıştır. Sonuç bildirilecektir.",
          "request_failed":"{{ site }} sahasının kapısı açılamamıştır. Destek için lütfen {{ support_line }} numarasını arayınız.  İşlem Numarası: {{ request_id }}",
          "request_completed":"{{ site }} sahasındaki kapı açılmıştır. Kapatmak için {{ site }} CIKIS ve işlemi yazarak 4549’a gönderebilirsiniz."
        }
      }

  :<json string support_line: Destek hattı numarası
  :<json array templates: SMS metinleri için kullanılacak taslaklar

  Cevap

    .. sourcecode:: http
    
      HTTP/1.1 200 OK
      Content-Type: application/json
  
      {
        "support_line":"05320000000",
        "templates":{
          "unauthorized_person":"Kullanıcı doğrulanamadı. Yetkili bir kullanıcı ile giriş yapabilirsiniz.",
          "wrong_format":"Girilen mesaj yanlıştır. Giriş yapmak istediğiniz sahanın adını yazıp boşluk bıraktıktan sonra GIRIS ve sonrasında giriş nedeninizi yazarak 4549’a gönderiniz.",
          "site_not_found":"Saha bilgisi hatalı. Destek için lütfen {{ support_line }} numarasını arayınız. İşlem Numarası: {{ request_id }}",
          "access_authorized":"{{ site }} sahasına girişiniz onaylanmıştır.",
          "access_deauthorized":"{{ site }} sahasından çıkışınız onaylanmıştır.",
          "cannot_authorize_access":"{{ site }} sahasına girişiniz onaylanamamıştır. Destek için lütfen {{ support_line }} numarasını arayınız. İşlem Numarası: {{ request_id }}",
          "wait_for_unlock":"{{ site }} sahasının kapısının açılması için işlem başlatılmıştır. Sonuç bildirilecektir.",
          "request_failed":"{{ site }} sahasının kapısı açılamamıştır. Destek için lütfen {{ support_line }} numarasını arayınız.  İşlem Numarası: {{ request_id }}",
          "request_completed":"{{ site }} sahasındaki kapı açılmıştır. Kapatmak için {{ site }} CIKIS ve işlemi yazarak 4549’a gönderebilirsiniz."
        }
      }

  :status 200: İstek başarılı
  :status 403: Yönetici hakları gerekli
  :status 500: İstek yerine getirilemedi
  
  :>json string support_line: Destek hattı numarası
  :>json array templates: SMS metinleri için kullanılacak taslaklar