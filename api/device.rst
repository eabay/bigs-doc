.. _api-device:

Cihaz HTTP API
==============

:ref:`device` tarafından komut ve yapılandırma için sağlanan API.


.. contents:: Başlıklar
  :local:


Komut
-----

Cihazın yerine getirmesi için yapılan istekler. Komut işletildikten sonra :ref:`api-device-gateway` üzerinden :ref:`yanıtlanmalıdır <api-command_response>`.

.. http:post:: /

  İstek

    .. sourcecode:: http

      POST / HTTP/1.1
      Host: 10.x.x.x:6543
      Content-Type: application/json

      {
        "command": {
          "id": "",
          "cmd": "",
          "target": "",
          "parameter": ""
        }
      }

  :<header Host: ``IP:PORT`` Cihazlar GSM veri hattı ve VPN için farklı IP adreslerine sahiptir.  Port numarası olarak GSM veri hattı için ``6543``, VPN hattı için de ``8080`` kullanılır.
  :<header Content-Type: ``application/json`` Sorgu içeriği JSON formatındadır.
  
  :<json string id: :ref:`Cevabın <api-command_response>` takip edilebilmesi için verilen eşsiz değer.
  :<json string cmd: Komut tipi
  :<json string target: Komutun gönderildiği bileşen.
  :<json string parameter: Komut parametresi.

  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK

  :status 200: Komut işlenecek
  :status 400: İstek başarısız


.. _api-command-door:

Kapı Kilidi
***********
Kapı kilidi aç-kapa istekleri


Aç
++

.. http:post:: /

  .. sourcecode:: http

    POST / HTTP/1.1
    Host: 10.x.x.x:6543
    Content-Type: application/json

    {
      "command": {
        "id": "WmSZQ9xXFxIyW0Uu",
        "cmd": "open",
        "target": "door",
        "parameter": "1"
      }
    }


Kapat
+++++

.. http:post:: /

  .. sourcecode:: http

    POST / HTTP/1.1
    Host: 10.x.x.x:6543
    Content-Type: application/json

    {
      "command": {
        "id": "WmSZQ9xXFxIyW0Uu",
        "cmd": "open",
        "target": "door",
        "parameter": "0"
      }
    }


.. _api-command-light:

Işık
****
Işık aç-kapa istekleri


Aç
++

.. http:post:: /

  .. sourcecode:: http

    POST / HTTP/1.1
    Host: 10.x.x.x:6543
    Content-Type: application/json

    {
      "command": {
        "id": "WmSZQ9xXFxIyW0Uu",
        "cmd": "open",
        "target": "light",
        "parameter": "1"
      }
    }


Kapat
+++++

.. http:post:: /

  .. sourcecode:: http

    POST / HTTP/1.1
    Host: 10.x.x.x:6543
    Content-Type: application/json

    {
      "command": {
        "id": "WmSZQ9xXFxIyW0Uu",
        "cmd": "open",
        "target": "light",
        "parameter": "0"
      }
    }



Ayarlar
-------

Ayar gönderimi için genel olarak tasarlanmıştır. Şu anda sadece VPN ağ ayarları için kullanılmaktadır.

.. _api-network-settings:

Ağ Ayarları
***********

.. http:post:: /

  İstek

    .. sourcecode:: http

      POST / HTTP/1.1
      Host: 10.x.x.x:6543
      Content-Type: application/json

      {
        "settings": {
          "ethernet": {
            "ip": "10.x.x.x",
            "gateway": "10.x.x.x",
            "subnet_mask": "255.255.255.240"
          }
        }
      }

  :<header Host: Cihazın GSM veri hattı IP adresi ve port ``6543`` 
  :<header Content-Type: Sorgu içeriği JSON formatındadır.
  
  :<json string ip: Cihaz VPN IP adresi
  :<json string gateway: Varsayılan ağ geçidi için IP adresi
  :<json string subnet_mask: ``255.255.255.240`` Alt ağ maskesi

  Cevap

    .. sourcecode:: http

      HTTP/1.1 200 OK

  :status 200: Komut işlenecek
  :status 400: İstek başarısız
