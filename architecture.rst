Sistem Mimarisi
===============

BİGS farklı servislerin biraraya gelmesinden oluşan dağıtık bir mimariye sahiptir. Sistemin topolojisi genel olarak aşağıdaki gibidir.


.. figure:: /assets/topology.*
   
   **BİGS Topolojisi** *Mavi kutular sunucu platformundaki mikroservisleri, kırmızı kutular sistemin çalışması için gerekli olan diğer servisleri ifade eder.*


.. _components:

Bileşenler
----------

:ref:`device`

  Sensörler aracılığıyla topladığı verileri rapor ve alarm şeklinde sunucuya iletir, gönderilen komutları yerine getirir.

:ref:`service-device-gateway`

  Cihazlardan gönderilen mesajları doğrular ve sonraki işlemler için kuyruğa gönderir.

:ref:`service-application`

  Kuyruktaki mesajları işler ve içeriğine göre alarmlar üretir, varolan alarmları kaldırır ya da öntanımlı aksiyonları gerçekleştirir. SMS giriş süreci de bu servis tarafından yönetilir.

:ref:`service-ui`

  Sahaların durumları görüntülenir ve değişimlerden anlık olarak izlenebilir. Cihazlara komut gönderilebilir, süreçler gözlenebilir.

:ref:`service-video-recording`

  Hareket algılanması durumunda otomatik olarak sahadaki kameradan aldığı görüntüleri kaydeder.

:ref:`service-temip-notifier`

  Üretilen tüm alarmları TeMIP'e iletir.
  

.. _dependencies:

Yardımcı Servisler
------------------

BİGS platformunun sağlıklı bir şekilde çalışabilmesi için servis olarak çalışan aşağıdaki yazılımlara da ihtiyaç duyar.

:term:`NSQ` *Mesaj Kuyruğu*

  Tüm mikroservislerin birbiri ile iletişimi NSQ ile sağlanır.
  
:term:`MongoDB` *Veritabanı*

  Tüm kayıtlar MongoDB üzerinden barındırılmaktadır.

:term:`Redis` *Pub/Sub*

  Dağıtık yapı içerisinde uygulama servisinden birden fazla kopya çalıştırılması durumunda websocket oturum ve konfigürasyon bütünlüğü Redis'in Pub/Sub özelliği ile sağlanır.

:term:`Graylog` *Log*

  Tüm servislerden üretilen log kayıtlarına Graylog sayesinde ortak bir yerden erişilebilir.

**LDAP** *Giriş / Yetkilendirme*

  Uygulamaya erişim LDAP hesapları üzerinden sağlanır.

**SMS Webservis** *Kısa Mesaj Gönderimi*

  Saha ile saha girişi sürecinde geri bildirimler sms webservisi ile sağlanır.
