Loglama
=======
Servislerin log'ları hem standart çıktıya (stdout) yazılır hem de :abbr:`UDP (User Datagram Protocol)` üzerinden :ref:`Graylog <dependencies>`'a gönderilir. Loglama için :term:`bunyan` kütüphanesi kullanılmıştır. Log seviyesi :envvar:`LOG_LEVEL` değişkeni ile değiştirilebilir.


.. tip::
  Log'lar her ne kadar Graylog'la ortak bir merkezden erişilebilir olsa da bazı durumlarda stdout izlenmek zorunda olunabilir. Böyle durumlar için çıktıları daha okunabilir kılmak için bunyan kurduktan sonra komut satırı aracını kullanabilirsiniz.
  
   .. code-block:: bash
  
     $ npm i -g bunyan
     $ npm start | bunyan
