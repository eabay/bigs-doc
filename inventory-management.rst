:orphan:

Envanter Yönetimi
=================

BİGS platformunun çalışması için gerekli saha, cihaz ve SMS'le giriş yetki listesi Excel formatında hazırlanan bir dosya ile içe aktarılabilir. Bu sayfa :download:`örnek dosya </assets/import-sample.xlsx>` içerisinde yer alan alanların ne anlama geldiklerini açıklamaktadır.


Bölümler
--------

Sahalar (site)
**************

.. csv-table::
   :header-rows: 1
   :stub-columns: 1
   
   İsim                       , Örnek         , Açıklama
   id                         , AKCAM         , Site ID
   region                     , IZMIR         , Bölge
   emg_name                   , IZMKAKCAMASAK , EMG Name
   name                       , KARABURUN     , Saha adı
   loc.lat:dms [#loc]_        , 38°35`36.8"   , Enlem
   loc.lng:dms [#loc]_        , 26°33`13.5"   , Boylam
   network.ip.device          , 10.67.132.XXX , Cihaz IP adresi
   network.ip.internal_camera , 10.67.132.XXX , İç kamera IP adresi
   network.ip.external_camera , 10.67.132.XXX , Dış kamera IP adresi
   network.gateway            , 10.67.132.XXX , Varsayılan ağ geçidi


Cihazlar (device)
*****************

.. csv-table::
   :header-rows: 1
   :stub-columns: 1
      
   İsim   , Örnek                               , Açıklama
   id     , 14016003-ae3d9508-53a59212-f5001c84 , Chip ID
   msisdn , 534XXXXXXX                          , Telefon Numarası
   ip.apn , 10.126.23.XXX                       , GSM ağından alınan IP adresi
   region , ANTALYA                             , Bölge
   


SMS'le Girişe Yetkili Kişiler (maintenance-worker)
**************************************************

.. csv-table::
   :header-rows: 1
   :stub-columns: 1
      
   İsim   , Örnek       , Açıklama
   phone  , 0534XXXXXXX , Telefon Numarası
   name   , Ali Veli    , İsim



.. warning:: Lütfen değerlerin hiçbirinde boşluk karakteri olmadığından emin olun!



.. [#loc] Koordinat bilgilerinde `DMS <http://www.geomidpoint.com/latlon.html>`_ formatı kullanılmaktadır. Eğer DD formatında giriş yapılacaksa kolon adındaki ``:dms`` ifadesi kaldırılmalıdır.

    .. csv-table::
      
       loc.lat:dms  , 38°35`36.8"
       loc.lat      , 38.593555555555554
