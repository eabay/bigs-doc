Kurulum
=======

BİGS platformu :term:`Node.js` üzerinde :term:`CoffeeScript` ve Javascript ile geliştirilmiştir. Servislerin çalıştırılabilmesi için öncelikle aşağıdaki gereksinimler sağlanmalıdır.

.. csv-table:: :header-rows: 1
   
   Gereksinim          , Test edilen son sürüm
   Node.js             , 0.10.36
   :term:`npm` [#npm]_ , 2.5.1
   CoffeeScript        , 1.9.0


Bunlara ek olarak bazı servisler fazladan gereksinimlere de ihtiyaç duyar.

.. csv-table:: :header-rows: 1
 
  Servis      , Gereksinim        , Test edilen son sürüm
  Video Kayıt , :term:`FFmpeg`    , git-2014-07-15-d595361 [#ffmpeg]_
  Arayüz      , :term:`Compass`   , 1.0.3
              , :term:`gulp`      , 3.8.11


Her bir servis proje dizininde ``services`` altında kendi klasörüne sahiptir.

::

  + docs
  + services
  |- app                  # Uygulama
  |- device-gateway       # Cihaz Ağ Geçidi
  |- temip-notifier       # TeMIP Alarm Gönderimi
  |- ui                   # Arayüz
  |- video-recording      # Video Kayıt

Kurulum için öncelikle istenen servisin dizinine gidilmelidir.

.. code-block:: bash

  $ cd services/app


Servise özel Node.js modül bağımlılıkları kurulmalıdır. [#npm_install]_

.. code-block:: bash

  $ npm install


.. important::

  Arayüz servisinde statik dosyaların tarayıcıdan sunulabilir hale gelmesi için öncelikle bazı işlemlerden geçirilmesi gerekir. Bu yüzden ``npm install`` yerine ``npm run build`` komutu çalıştırılmalıdır.


Tüm kurulumlar tamamlandıktan sonra servis başlatılabilir.
 
.. code-block:: bash

  $ npm start


.. admonition:: Servisleri arkaplanda çalıştırma

  Servisleri arkaplanda çalıştırabilmek için işletim sistemiyle gelen araçlar (runit, systemd vs.) kullanılabileceği gibi :term:`Supervisor` gibi yazılımlar da seçilebilir.

  .. code-block:: ini
    
    # Uygulama için örnek Supervisor yapılandırması
    
    [program:app]
    directory=<proje klasörü>/services/app
    command=npm -q start
    autostart=true
    autorestart=true
    stdout_logfile=/var/log/bigs/app/out.log
    stderr_logfile=/var/log/bigs/app/err.log


.. [#npm] npm Node.js kurulumu ile beraber gelmektedir.
.. [#ffmpeg] FFmpeg kurulumu `buradaki <https://trac.ffmpeg.org/wiki/CompilationGuide>`_ yönergelere göre yapılmıştır.
.. [#npm_install] Bazı modüller derleme gerektirdiği için kurulum yapılan sistemde geliştirme paketlerine ihtiyaç duyulacaktır. Örneğin Debian/Ubuntu için *build-essential* paketi kurulmalıdır.

