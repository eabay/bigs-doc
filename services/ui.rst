.. _service-ui:

Arayüz
======

:ref:`service-application` servisinin arayüzüdür, :ref:`sağladığı <api-application>` API'yi kullanılır. Ayrıca :ref:`kamera kayıtlarına <service-video-recording>` da bu servisten erişilebilir.

.. image:: /assets/ui.png


.. contents:: Başlıklar
  :local:


Arama
-----
Sahanın adını girerek arama yapılabilir. Yazarken otomatik tamamlama ile bulunan sahaların listesi görüntülenmeye başlanır.


Cihaz Kurulumu
--------------
Mesaj gönderen fakat herhangi bir sahaya atanmamış cihazlar sahalara eşleştirilebilir.


SMS Giriş İstekleri
-------------------
SMS ile yapılan saha giriş istekleri ve tüm alt süreçleri buradan görüntülenebilir.

Ayarlar
-------
Sadece yönetici yetkisindeki kullanıcıların görüntüleyebildiği, şu an için sadece SMS giriş süreçleriyle ilgili ayarların olduğu bölümdür.

İstatistikler
-------------
Kurulum yapılan, giriş yapılmış ve alarm olan saha sayıları yer alır.


Harita
------
Sahalar harita üzerinde anlık güncellenen durumlarıyla beraber görüntülenirler. Sahaların durumları renklerle ifade edilir. Kamera olan sahalarda ufak bir kamera simgesi görünür.

.. csv-table::
  :header-rows: 1
  
  Renk, Durum
  Kırmızı, Alarm var
  Mavi, Alarm yok
  Turuncu, Saha girişi etkinleştirilmiş

Saha Detayları
--------------

Saha Bilgileri
**************

Sahanın adı (Site ID), uzun adı, bölgesi ve son güncellendiği tarihi içerir.

.. _ui-site-access:

Saha Girişi
***********
Saha girişi etkinleştirilebilir ya da kaldırılabilir.

Cihaz Bilgileri
***************
Cihazın chip ID, APN IP, MSISDN, ve firmware versiyonu bilgileri görüntülenir. Cihazı yeniden başlatma ve firware güncelleme istekleri de bu bölümde yer alır.

Bileşenler
**********
Işık ve kapı kilidi açılıp kapatılabilir. Kapı kilidi için SMS ile komut gönderme opsiyonu da mevcuttur. Ayrıca cihaza takılı :ref:`bileşenler <device-components>` ve bunların anlık durumları görüntülenir.

.. csv-table:: Sensör değerleri
  :header-rows: 1
  
  Durum             , Açıklama
  Durum bilinmiyor  , Henüz bir durum bildirmemiş. İlk rapor ya da alarmla durum güncellenecek.
  Alarm             , Sensör tetiklenmiş.
  Kısa Devre
  Kablo Kesik


Kamera Görüntüleri
******************
Bağlı olan kameralara direkt bağlantı ile anlık olarak saha izlenebilir.

.. note:: Kamera görüntüleri MJPEG formatıyla tarayıcıda gösterilir.

  :Dış Kamera: ``http://admin:*****@10.x.x.x/ipcam/mjpeg.cgi``
  :İç Kamera: ``http://10.x.x.x/asp/video.cgi?profile=4&resolution=640x360``

Enerji
******
Sahanın enerji tipi ve durumu ile cihaz pili durumunu içerir.

Saha enerji tipleri
  - Rectifier (-48)
  - Akü (+24)

Saha enerji durumları
  - Doluluk yüzdesi [#charge_ratio]_
  - Akü durumu kritik seviyede.
  - Aküler bitmiş durumda. Cihaz pilden çalışıyor.

Cihaz pil durumları
  - Doluluk yüzdesi [#charge_ratio]_
  - Cihaz pili kritik seviyede.
  - Cihaz pili arızalı ya da takılı değil. Lütfen pil durumunu kontrol edin.


Saha Kayıtları
**************
Saha giriş-çıkışları ve alarm kayıtlarını içerir.

Girişler
  Kanal, işlemi yapan ve sebep bilgileri yer alır.
  
  .. csv-table:: Kanallar

    WEB, İşlem uygulama üzerinden yapılmış.
    SMS, SMS ile istekte bulunulmuş.
    BİGS, Uygulama tarafından yapılmış. Sadece otomatik çıkışlarda görünür.
  
  İşlemi yapan kişinin adı soyadı yanında WEB kanalından yapılan işlemlerde kullanıcı adı, SMS ile yapılan işlemlerde telefon numarası görüntülenir.
  
  Sebep bilgisi sadece SMS ile yapılan girişlerde mevcuttur.

Alarmlar
  Alarm başladığı ve sona erdiği tarihler, geçen süre ve kaç kere tekrarlandığı bilgileri yer alır.


Video Kayıtları
---------------
:envvar:`VIDEO_STORAGE_PATH` ile belirtilen video kayıtlarına ``/video-records`` ile erişilebilir. Verilen dizindeki dosyalar basitçe listelenir. Herhangi bir yetkilendirme söz konusu değildir.


.. [#charge_ratio] Kritik bir durum olmadığını gösterir.
