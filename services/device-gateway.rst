.. _service-device-gateway:

Cihaz Ağ Geçidi
===============

:ref:`Cihazlardan <device>` gönderilen mesajların karşılandığı servistir.

Sağladığı :ref:`HTTP API <api-device-gateway>` üzerinden gönderilen mesajlar doğrulanır, cihazın bildirdiği gönderim tarihi standard formata çevrilir, istek yaptığı IP adresi ve servis tarafından sorgu için verilen :abbr:`UUID (Universally Unique Identifier)` de mesaja eklenerek kuyruğa gönderilir.

Doğrulamadan geçemeyen mesajlar reddedilir. Herhangi bir problemden kuyruğa gönderilemeyen mesajlar aynı istek içerisinde cihaza cevap içerisinde iletilir. Bu mesajlar cihaz tarafından tekrar iletilmelidir.

.. image:: /assets/device-message-flow.*
