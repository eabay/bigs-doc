.. _service-video-recording:

Video Kayıt
===========

Kuyrukta bekleyen video kayıt isteklerini işleyen servistir.

Kayıt istekleri saha adı, bağlanılacak kameranın tipi ve adresi bilgilerinden oluşur. Kamera adresine bağlanılarak :envvar:`RECORD_DURATION` değerinde belirtilen süre kadarlık kaydı :envvar:`STORAGE_PATH` ile belirlenen dizine kaydeder. Aynı anda kaç kayıt yapılacağı (:envvar:`MAX_IN_FLIGHT`) ve başarısız denemelerde kaç kere tekrar deneneceği (:envvar:`MAX_ATTEMPTS`) gibi ayarlar da yapılabilir.

.. seealso:: Ayarlarla ilgili açıklamalar için yapılandırma dokümanının :ref:`servisle ilgili bölümünü <conf-video-recording>` kontrol edebilirsiniz.


Video Formatı
-------------
Görüntüler diske **H264** formatında kaydedilir.

Dış kamerada görüntü formatı H264 olduğundan format dönüşümüne ihtiyaç yoktur.

.. code-block:: bash

  # Kuyruktan okunan mesaj
  # {"url": "rtsp://admin:*****@10.x.x.x/main", "site": "GEBMF", "source": "external_camera"}
  
  # Komut
  $ ffmpeg -i rtsp://admin:*****@10.x.x.x/main -y -vcodec copy -t 60 /data/2015/03/06/GEBMF/external_camera/GEBMF-20150306102033-external_camera-07f6813b3bb28000.mp4


İç kamerada ise format MJPEG olduğundan kaydedilmeden önce H264 formatına çevrilir.

.. code-block:: bash

  # Kuyruktan okunan mesaj
  # {"url": "http://10.x.x.x/asp/video.cgi?profile=4", "site": "VETAL", "source": "internal_camera"}

  $ ffmpeg -f mjpeg -r 6 -i http://10.x.x.x/asp/video.cgi?profile=4 -y -vcodec libx264 -t 60 /data/2015/03/06/VETAL/internal_camera/VETAL-20150306110542-internal_camera-07f68b971bb28000.mp4


.. note:: Kayıt için :term:`FFmpeg` yazılımı kullanılır. Yazılım bir yardımcı kütüphan aracılığıyla oluşturulan alt proses yönetildiği için örnekler direkt olarak :command:`ffmpeg` komutu olarak verilmiştir.


Uyarılar
--------
Video kayıt servisi, üzerinde en hassas kaynak planlaması gerektiren servistir. İstek sayısını karşılayabilecek kadar yeterli sayıda proses başlatılmasının yanında CPU ve disk kapasitesi anlamında da planlama yapılmalıdır.

.. topic:: Kayıt isteklerine yanıt veren proses sayısı
  
  Servisin birden fazla kopya şeklinde başlatılabilir ya da proses başına aynı anda yapabileceği kayıt sayısı :envvar:`MAX_IN_FLIGHT` değeri ile değiştirilebilir. İki strateji arasındaki seçimde ilkine doğru eğilim gösterilmesi tavsiye edilen kullanım şeklidir. Böylede ortaya çıkabilecek sorunlarda sadece sorunun olduğu prosesi durdurmak daha az veri kaybıyla sonuçlanacaktır.

.. topic:: İç kamera görüntüsünde format değişimi

  Format değişimi CPU kullanımı çok yüksek bir işlemdir. Aynı anda yapılan kayıt sayısının yaratacağı yük kayıtlarda kare kaymalarına ve kayıplarına, hatta oynatılamayan dosyaların oluşmasıyla kayıt kaybına sebep olabilir.

.. topic:: Disk kapasitesi

  Servis, kayıt yapılan diskle ilgili herhangi bir yönetim becerisiye sahip değildir. Disk kapasitesinin yeterli olmaması kayıtların başarısız olması demektir.

.. topic:: Diskin yazma hızı

  Diskin yazma hızı aynı anda yapılabilecek kayıt sayısına etken bir parametredir. NFS tipindeki disklerde buna ağın hızı da eklenir.
