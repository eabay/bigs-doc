.. _service-application:

Uygulama
========

BİGS platformunun karar merkezi olan servistir. :ref:`service-device-gateway` tarafından kuyruğa alınan mesajları işler ve içeriğine göre alarmlar üretir, varolan alarmları kaldırır ya da öntanımlı aksiyonları gerçekleştirir. SMS giriş süreci de bu servis tarafından yönetilir.


.. contents:: Başlıklar
  :local:


Cihaz Mesajının İşlenmesi
-------------------------
Kuyruktan gelen mesajda öncelikle gönderen cihazın sistemde kayıtlı olup olmadığına bakılır. Cihaz, mesaj içinde gelen ve chip id olarak bilinen UUID ile ayırt edilir. Uygulama tarafından bilinmeyen cihazlardan gelen mesajlar direkt reddedilir. Sistemde kayıtlı olan cihaz bir sahaya eşleştirilmişse mesaj işlenir, aksi takdirde :ref:`kurulum <provisioning>` süreci başlatılır.

Gelen mesajın içeriği ile öncelikle cihazın üzerindeki bileşenlerin durumları güncellenir.

Sensörler
*********
Sensörlerin durumları :ref:`rapor <api-report>` ya da :ref:`alarmlardan <api-event-alarms>` gelen değerler ile güncellenir. Alabilecekleri değerler ``0-3`` arasında olabilir.

Işık
****
:ref:`api-report`, :ref:`api-event-lights_turned_on` ve :ref:`api-event-lights_turned_off` olayları ile gelen değer kullanılır. Bununla birlikte cihaza gönderilen :ref:`komutun <api-command-light>` :ref:`yanıtına <api-command_response>` göre de durum güncellenir. Alabileceği değerler ``0 - Kapalı`` ve ``1 - Açık`` olabilir.

Kapı Kilidi
***********
:ref:`api-report` ile gelen değer kullanılır. Bununla birlikte cihaza gönderilen :ref:`komutun <api-command-door>` :ref:`yanıtına <api-command_response>` göre de durum güncellenir. Alabileceği değerler ``0 - Kapalı`` ve ``1 - Açık`` olabilir.

Kutu Kapağı
***********
:ref:`api-report`, :ref:`api-event-box_opened` ve :ref:`api-event-box_closed` olayları ile gelen değer kullanılır. Alabileceği değerler ``0 - Kapalı`` ve ``1 - Açık`` olabilir.

.. note:: :ref:`api-event-box_closed` olayı kutu kapağının durumunu güncellemekle birlikte cihaza bağlı güncel bileşen listesini de gönderir. Bu liste ile :ref:`cihaz bileşenleri güncellenir <components-update>`. Bununla birlikte :ref:`cihaza ağ ayarlarının gönderilmesi <network-settings>` de bu olayla tetiklenir.

Enerji Durumu
*************
:ref:`api-report` ile gelen enerji bilgileri ADC olarak sağladığından öncelikle anlaşılabilir bir formata çevrilir.

Sahanın enerji tipi, voltajı ve doluluk yüzdesi hesaplanır:

  .. sourcecode:: javascript

    function getPowerInfo(power) {
        var type = null;
        var voltage = null;
        var highBoundaryVoltage = 48;
        var lowBoundaryVoltage = 30;
        var chargeRatio = 0;
        var adc1 = parseFloat(power.adc1);
        var adc2 = parseFloat(power.adc2);

        if (adc2 < 1.8) {
            var v = Math.abs(adc1 * 21 - 66);

            if (v > 24) {
                type = '-48';
                highBoundaryVoltage = 48;
                lowBoundaryVoltage = 30;
                voltage = v;
            }
        } else {
            type = '+24';
            highBoundaryVoltage = 24;
            lowBoundaryVoltage = 18;
            voltage = adc2 * 10;
        }

        if (voltage > highBoundaryVoltage) {
            chargeRatio = 100;
        } else if (voltage < lowBoundaryVoltage) {
            chargeRatio = 0;
        } else {
            chargeRatio = (voltage - lowBoundaryVoltage) * 100 / (highBoundaryVoltage - lowBoundaryVoltage);
        }

        return {
            voltageType: type,
            voltage: Math.ceil(voltage),
            charge_ratio: Math.ceil(chargeRatio)
        };
    }

Cihazın pil voltajı ve doluluk oranı hesaplanır:

  .. sourcecode:: javascript

    function getBatteryInfo(power) {
        var voltage = parseFloat(power.adc3) * 1.5;
        // http://www.candlepowerforums.com/vb/showthread.php?115871-Li-Ion-State-of-Charge-and-Voltage-Measurements&p=2440539&viewfull=1#post2440539
        var chargeRatio = (0.1966 + Math.sqrt(0.0387 - (1.4523 * (3.7835 - voltage)))) * 100;

        if (chargeRatio > 100) {
            chargeRatio = 100;
        }

        if (_.isNaN(chargeRatio)) {
            chargeRatio = 0;
        }

        return {
            voltage: parseFloat(voltage.toFixed(2)),
            charge_ratio: Math.ceil(chargeRatio)
        };
    }


.. _alarms:

Alarmlar
--------
Cihaz bilgilerinin güncellenmesinin ardından bileşenlerin değerlerine göre alarmlar üretilebilir.


DEVICE_BOX_OPEN
***************
Kutu kapağı açık alarmı. Kutu kapağı değeri ``1`` ise tetiklenir.


INTERNAL_MOTION
***************
İç hareket alarmı. İç hareket sensörü değeri ``1`` ise tetiklenir.


EXTERNAL_MOTION
***************
Dış hareket alarmı. Dış hareket sensörü değeri ``1`` ise tetiklenir.


SHOCK
*****
Darbe alarmı. Darbe sensörü değeri ``1`` ise tetiklenir.


FLOOD
*****
Su baskını alarmı. Su baskını sensörü değeri ``1`` ise tetiklenir.


FIRE
****
Yangın alarmı. Duman ve ısı sensörü değeri ``1`` ise tetiklenir.


SHORT_CIRCUIT_*
***************
``SHORT_CIRCUIT_INTERNAL_PIR`` ``SHORT_CIRCUIT_EXTERNAL_PIR`` ``SHORT_CIRCUIT_WATER_SENSOR`` ``SHORT_CIRCUIT_FIRE_SENSOR`` ``SHORT_CIRCUIT_SHOCK_SENSOR``

Sensör kısa devre alarmı. Sensör değeri ``2`` ise tetiklenir.


CABLE_CUT_*
***********
``CABLE_CUT_INTERNAL_PIR`` ``CABLE_CUT_EXTERNAL_PIR`` ``CABLE_CUT_WATER_SENSOR`` ``CABLE_CUT_FIRE_SENSOR`` ``CABLE_CUT_SHOCK_SENSOR``

Sensör kablosu kesik alarmı. Sensör değeri ``3`` ise tetiklenir.

-------

Yukarıdaki alarmlar ilişkili oldukları bileşenin değeri ``0`` olduğunda kaldırılırlar.

-------

UNAUTHORIZED_ACCESS
*******************
Dış hareket algılanması durumunda **1 dakika** boyunca giriş talebi yapılmaması ya da iç hareket algılanması durumlarında üretilir. Hareket alarmlarının hepsi kalktığında ya da :ref:`site-access` etkileştirildiğinde kaldırılır.


REPORT_DELIVERY_PROBLEM ve DEVICE_UNREACHABLE
*********************************************
Bu iki alarm birbiriyle yakından ilişkilidir. **15 dakika** boyunca rapor mesajı göndermeyen cihazlara RESET komutu gönderilmeye çalışılır. Gönderimin başarısız olması durumunda *DEVICE_UNREACHABLE* alarmı üretilir. Gönderim başarılı olmasına rağmen halen rapor mesajı gönderilmemesi durumunda ise *REPORT_DELIVERY_PROBLEM* alarmı üretilir. Cihaz tekrar rapor gönderdiğinde bu alarmlar kaldırılır.

.. note:: :term:`TeMIP` ile iletişim yönteminin sadece alarm gönderimi olması sebebiyle gerçek bir alarm olmamalarına rağmen bilgilendirme amaçlı kullanılan alarmlar da mevcuttur.

          :HEARTBEAT: Sistemin ayakta olduğuna dair webservise belli aralıklarla gönderilen alarmdır.
          :ACCESS_GRANTED: Saha giriş yapıldığında üretilir, çıkış yapıldığında kaldırılır.


.. _provisioning:

Cihaz Kurulumu
--------------
Henüz bir sahaya atanmamış cihazların saha eşleştirmeleri yapılır. Cihazlar gibi sahalar da uygulamada tanımlı olmalıdır.

.. attention:: Sadece aynı bölgede tanımlı olan cihaz ve sahalar eşleştirilebilir.


.. _components-update:

Bileşenlerin Güncellenmesi
--------------------------
Cihaz üzerindeki :ref:`bileşenler <device-components>` :ref:`api-event-box_closed` mesajı ile güncellenir. Cihazda hangi bileşenlerin olduğu alarm üretimi ve kapı kilidi komutlarında dikkate alındığı için önemlidir.


.. _network-settings:

Ağ Ayarlarının Gönderilmesi
---------------------------
:ref:`api-event-box_closed` mesajı alındığında eğer eşleştirilen sahaya ait VPN ayarları mevcutsa bu bilgiler cihaza :ref:`gönderilir <api-network-settings>`.


Kamera Kaydı
------------
INTERNAL_MOTION_ alarmında iç kamera, EXTERNAL_MOTION_ alarmında da dış kamera kaydı isteği :ref:`service-video-recording` servisine aktarılmak üzere kuyruğa gönderilir. Alarm tekrarları da bunlara dahildir.

Kameraların sağlıklı bir şekilde çalışıp çalışmadığına dair herhangi bir kontrol yapmaz. Sistemde kameraya ait bir IP adresi olduğu sürece kamera kaydı isteği gönderilir.


Giriş (Authentication)
----------------------
Sağlanan LDAP yapılandırma bilgileriyle (:envvar:`LDAP_URL`, :envvar:`LDAP_BASEDN`, :envvar:`LDAP_USERNAME`, :envvar:`LDAP_PASSWORD`) bağlanılan sunucudaki tüm kullanıcılar kullanıcı adı ve şifresisi ile servise erişebilir. Girişin ardından sürekli olarak açık kalan bir oturum yoktur. Sistem token bazlı çalışır. Giriş sorgusu cevabında sağlanan token daha sonraki sorgulara dahil edilir. Token üretirken kullanılan şifre :envvar:`JWT_SECRET` ile değiştirilebilir.


Yetkilendirme (Authorization)
-----------------------------
Her bir saha bir bölgeye aittir. Her bir kullanıcı da bir ya da birden fazla kullanıcı grubuna dahildir. Kullanıcı gruplarına bölgelere erişim hakkı tanımlanır. Kullanıcı grupları birden fazla bölgeye erişim hakkına sahip olabilir. Böylece kullanıcılar dahil oldukları grublara erişim hakkı verilen bölgelerdeki sahalara yetkilendirilmiş olur.

.. important:: Bir kullanıcının sahaya erişebilmesi demek sahaya tam yetkili olarak müdahale edebilmesi demektir.

Kullanıcıların gruplara dahil edilmesi giriş sırasında okunan LDAP profil bilgisi üzerinden otomatik olarak yapılır. Profildeki LDAP grupları okunarak ``INFO-BIGS-`` ile başlayanlara dahil olanlar uygulama tarafındaki gruplara dahil edilir.

.. topic:: Örnek

  Uygulamada ``NOM`` adında bir kullanıcı grubu olsun ve ``ASYA`` bölgesine dahil olan sahalara erişebilsin. ``EXT999999`` kullanıcı adıyla giriş yapan kişinin LDAP profilinde ``INFO-BIGS-NOM`` adlı bir grup varsa otomatik olarak ``NOM`` grubuna dahil edilecektir. Böylece ``ASYA`` bölgesindeki sahalara erişebilir.


.. _site-access:

Saha Girişi
-----------
Sahaya fiziksel olarak yapılacak girişlerde sahada öncelikle giriş etkinleştirilmelidir. Bu SMS ile ya da WEB üzerinden yapılabilir.

SMS ile
*******
``4549``'a ``SITEID (GIRIS|CIKIS) SEBEP`` formatında mesaj atılması vasıtasıyla giriş talebinde bulunulabilir. SMS sağlayıcı tarafından mesaj BİGS platformuna :ref:`ulaştırıldıktan <api-dispatchsms>` sonra numaranın talep yetkisi ve mesajın formatıyla başlayan bir :download:`süreç </assets/sms-access-flow.png>` başlatılır.

WEB ile
*******
Arayüz :ref:`üzerinden <ui-site-access>` saha girişi yapılabilir. SMS ile yapılan girişlerden farklı olarak kapı kilidi için herhangi bir aksiyon alınmaz.


.. _temip-integration:

TeMIP Entegrasyonu
------------------
Tüm alarmlar :ref:`service-temip-notifier` servisine aktarılmak üzere mesaj kuyruğuna gönderilir. Mesajlar :ref:`alarm tipi <alarms>`, sahanın EMG adı, oluştuğu tarih ve varsa özel alanları içerir. Gönderim için aşağıdaki süreç uygulanır:

#. Alarm oluştuğunda hemen kuyruğa gönderilir.
#. Alarm tekrarları gönderilmez.
#. Alarm durumu ortadan kalktığında bildirim hemen yapılmaz, **15 dakikalık** bir geri sayım başlatılır. Geri sayımın sonunda alarmın kalktığı bildirimi kuyruğa gönderilir. Geri sayım devam ederken aynı alarmdan tekrar oluşması durumunda alarm gönderilmez, geri sayım süreci de ortadan kaldırılır.
