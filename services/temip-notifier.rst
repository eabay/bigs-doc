.. _service-temip-notifier:

TeMIP Alarm Gönderimi
=====================

:ref:`Uygulama <temip-integration>` tarafından kuyruğa alınan alarm gönderim isteklerini işleyen servistir.

:envvar:`TEMIP_URL` değerinde belirtilen webservise alarm gönderim sorgusu yapılır. Sorgunun zaman aşımı süresi (:envvar:`TIMEOUT`), aynı anda kaç kayıt yapılacağı (:envvar:`MAX_IN_FLIGHT`) ve başarısız denemelerde kaç kere tekrar deneneceği (:envvar:`MAX_ATTEMPTS`) gibi ayarlar da yapılabilir.

.. seealso:: Ayarlarla ilgili açıklamalar için yapılandırma dokümanının :ref:`servisle ilgili bölümünü <conf-temip-notifier>` kontrol edebilirsiniz.
