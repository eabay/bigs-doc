Baz İstasyonu Güvenlik Sistemi
==============================

.. only:: html

  .. image:: assets/logo.*
     :alt: BİGS Logo
     :width: 30%
     :align: right

Baz İstasyonu Güvenlik Sistemi (BİGS), sahalara kurulan cihazlar ve bu cihazların iletişim halinde olduğu sunucu platformundan oluşan bir güvenlik çözümüdür. BİGS ile sahaların durumu gözlenebilir, alarm durumlarında aksiyon alınabilir ve sahalara fiziksel girişlerde yetkilendirme sağlanabilir.

Bu dokümantasyonda sunucu platformuyla ilgili aşağıdaki başlıklarında bilgiler yer almaktadır.

* Sistem Mimarisi
* Servisler
* Süreç akışları
* Kurulum
* Yapılandırma


.. toctree::
   :hidden:
   :glob:

   architecture
   device
   services/index
   installation
   configuration
   logging
   api/index
   glossary
