Sözlük
======

.. glossary::
  :sorted:

  Compass_

    CSS kütüphanesi.


  CoffeeScript_

    Kaynaktan kaynağa derleme yapan ve JavaScript kodu üreten bir geliştirme aracı.


  Node.js_

    Yüksek performanslı ağ uygulamaları geliştirme imkanı sunan bir platformu.


  npm_

    :term:`Node.js` paket yöneticisi.


  FFmpeg_

    Ses ve görüntüleri kayıt etmeye, formatlarını dönüştürmeye yarayan araç.


  Supervisor_

    Proses yönetim sistemi.


  gulp_

    Dosya sistemi tabanlı işlerde Make benzeri otomasyon sağlayan bir :term:`Node.js` modülü.


  NSQ_

    Ölçeklenebilir mesaj kuyruğu uygulaması.


  MongoDB_

    Ölçeklenebilir, doküman tabanlı NoSQL veritabanı uygulaması.


  Redis_

    Anahtar-değer (key-value) veritabanı.


  Graylog_

    Log kayıtlarını toplama, indeksleme ve analiz etmeyi sağlayan bir log yönetim platformu.


  bunyan_

    :term:`Node.js` için bir JSON loglama kütüphanesi.

  JWT_

    JSON Web Token

  TeMIP

    Turkcell'de saha alarmları için kullanılan merkezi platform.


.. _NSQ: http://nsq.io
.. _MongoDB: https://www.mongodb.org
.. _Redis: http://redis.io
.. _Graylog: https://www.graylog.org
.. _CoffeeScript: http://coffeescript.org
.. _npm: https://www.npmjs.com
.. _Node.js: https://nodejs.org
.. _FFmpeg: http://ffmpeg.org
.. _Compass: http://compass-style.org
.. _Supervisor: http://supervisord.org
.. _gulp: http://gulpjs.com
.. _bunyan: https://github.com/trentm/node-bunyan
.. _JWT: https://developer.atlassian.com/static/connect/docs/concepts/understanding-jwt.html