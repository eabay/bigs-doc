.. _device:

Cihaz
=====

Sahaya kurulan cihazın temel işlevi, sensörler aracılığıyla topladığı verileri belli aralıklarla rapor, sensörlerin tetiklenmesi durumunda alarm ve gönderilen komutların sonuçları hakkında komut yanıtı mesajlarını :ref:`HTTP API <api-device-gateway>` üzerinden sunucuya bildirmektir. Ayrıca sağladığı :ref:`HTTP <api-device>` ve  :ref:`SMS <api-device-sms>` API'leri ile gönderilen komutları işleyebilir.

.. contents:: Başlıklar
   :local:

.. _device-components:

Bileşenler
----------
Cihaz çeşitli sensörlerle beraber kapı kilidi ve konteyner ışığı yönetimi gibi bileşenlere sahip olabilir.

.. _device-sensors:

Sensörler
*********

:Hareket:      İç ve dış olmak üzere 2 adet PIR sensörü olabilir. Hareket algılandığında alarm üretir.
:Darbe:        Kapı ya da kapalı saha (konteyner) bir darbe aldığında alarm üretir.
:Su Baskını:   Konteyner su ile dolmaya başladığında alarm üretir.
:Duman ve Isı: Konteyner içinde bir tutuşma sonucu duman çıkması ve ısının yükselmesi durumunda alarm üretir.

Işık
****
Konteyner iç aydınlatması cihaz tarafından kontrol edilir, sunucu tarafından ışık açılıp kapatılabilir.

Kapı Kilidi
***********
Cihaz kutusunda yer alan sürgülü kilit sahaya girişleri kontrol altına alır. Kapı kilidi sunucu tarafından açılıp kapatılabilir.


Mesajlar
--------

Rapor
*****
Tüm bileşenlere ait durumlar, kutu kapağı açık olup olmadığı, saha aküsü ve cihaz pili gerilimleri ile firmware versiyonu bilgilerini içerir. Dakikada bir gönderilir.

Olay
*****
Bağlı sensörlerden birinde, kutu kapağında ya da ışıkta oluşan durum değişimini içerir.

Komut Yanıtı
************
Sunucu tarafından gönderilen komutun başarılı olup olmadığı, başarısızsa neden bilgisini içerir.
