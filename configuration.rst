Yapılandırma
============

Servisler ortam değişkenleri (environmental variables) ile yapılandırılabilir.


.. contents:: Başlıklar
  :local:


.. _conf-common:

Ortak Değişkenler
-----------------

Aşağıdaki değişken tanımları servisler arasında ortak olarak kullanılır.


.. envvar:: NODE_ENV

  Servisin başlatıldığı ortam.

  :Olası değerler: ``production`` ``development``
  :Varsayılan: ``production`` [#env_default]_


.. envvar:: LOG_LEVEL

  Log seviyesi.

  :Olası değerler: ``fatal`` ``error`` ``warn`` ``info`` ``debug`` ``trace``
  :Varsayılan: ``info``


.. envvar:: NSQD_URL
  
  :ref:`NSQ <dependencies>` servisinin başlatıldığı sunucu adresi.
  
  :Format: ``hostname:port``
  :Örnek: ``10.x.x.x:4150``


.. envvar:: GRAYLOG_URL

  :ref:`Graylog <dependencies>` sunucusu GELF UDP adresi.

  :Format: ``hostname:port``
  :Örnek: ``10.x.x.x:12201``


.. envvar:: PORT

  Servisin başlatılacağı port numarası.

  :Varsayılan: [#custom_default]_


.. envvar:: MAX_IN_FLIGHT

  :ref:`Mesaj kuyruğundan <dependencies>` aynı anda kabul edilecek mesaj sayısı. Değişkeni kullananan servislerde bu aynı anda yapılacak işlem sayısı anlamına gelir.

  :Format: ``sayı``
  :Varsayılan: [#custom_default]_


.. envvar:: MAX_ATTEMPTS

  Başarısız denemeler için tekrar sayısı.

  :Format: ``sayı``
  :Varsayılan: [#custom_default]_


Servislere Özel Değişkenler
---------------------------

.. _conf-device-gateway:

Cihaz Ağ Geçidi
***************

:envvar:`NODE_ENV`, :envvar:`LOG_LEVEL`, :envvar:`NSQD_URL`, :envvar:`GRAYLOG_URL` ve ve :envvar:`PORT` ``3000`` ile birlikte aşağıdaki değişkenler kullanılır.

.. envvar:: MESSAGE_TIMEZONE

  Cihaz mesajında yer alan tarihin saat dilimi.

  :Olası değerler: `Saat dilimleri`_
  :Varsayılan: ``Europe/Istanbul``


.. _Saat dilimleri: http://en.wikipedia.org/wiki/List_of_tz_database_time_zones


.. _conf-application:

Uygulama
********

:envvar:`NODE_ENV`, :envvar:`LOG_LEVEL`, :envvar:`NSQD_URL`, :envvar:`GRAYLOG_URL` ve :envvar:`PORT` ``5000`` ile birlikte aşağıdaki değişkenler kullanılır.


.. envvar:: REDIS_URL

  :ref:`Redis <dependencies>` sunucusu adresi.

  :Format: ``hostname:port``
  :Örnek: ``10.x.x.x:6379``


.. envvar:: MONGODB_URL

  :ref:`MongoDB <dependencies>` bağlantı cümlesi.

  :Format: `İlgili MongoDB dokümantasyonu`_
  :Örnek: ``mongodb://10.x.x.x/bigs``

.. _İlgili MongoDB dokümantasyonu: http://docs.mongodb.org/manual/reference/connection-string/


.. envvar:: LDAP_URL

  :ref:`LDAP <dependencies>` sunucu adresi.

  :Format: ``hostname:port``
  :Örnek: ``10.x.x.x:6379``


.. envvar:: LDAP_BASEDN

  :ref:`LDAP <dependencies>` Base DN.
  
  :Varsayılan: ``DC=turkcell,DC=entp,DC=tgc``


.. envvar:: LDAP_USERNAME
  
  :ref:`LDAP <dependencies>` kullanıcı adı.


.. envvar:: LDAP_PASSWORD
  
  :ref:`LDAP <dependencies>` kullanıcı şifresi.


.. envvar:: JWT_SECRET

  :term:`JWT` için kullanılacak şifre.

  :Format: ``metin``


.. envvar:: DISPATCHSMS_ROUTE

  :ref:`api-dispatchsms` için kullanılan *URL path*
  
  :Varsayılan: ``/sms-gateway/DispatchSMS``


.. _conf-ui:

Arayüz
******

:envvar:`NODE_ENV` ve :envvar:`PORT` ``4000`` ile birlikte aşağıdaki değişkenler kullanılır.


.. envvar:: SERVER_URL
  
  :doc:`/services/application` servisinin adresi
  
  :Örnek: ``http://10.x.x.x:5000``


.. envvar:: VIDEO_STORAGE_PATH
  
  Video kayıtlarının dosya sisteminde bulunduğu yer.

  :Varsayılan: ``/video-records``


.. _conf-video-recording:

Video Kayıt
***********

:envvar:`NODE_ENV`, :envvar:`LOG_LEVEL`, :envvar:`NSQD_URL`, :envvar:`GRAYLOG_URL`, :envvar:`MAX_IN_FLIGHT` ``1`` ve :envvar:`MAX_ATTEMPTS` ``5`` ile birlikte aşağıdaki değişkenler kullanılır.


.. envvar:: RECORD_DURATION
  
  Video kayıt uzunluğu.

  :Format: ``süre`` [#duration]_
  :Varsayılan: ``1 minute``


.. envvar:: STORAGE_PATH
  
  Kayıtların dosya sisteminde kaydedileceği yer.

  :Varsayılan: ``/data``


.. _conf-temip-notifier:

TeMIP Alarm Gönderimi
*********************

:envvar:`NODE_ENV`, :envvar:`LOG_LEVEL`, :envvar:`NSQD_URL`, :envvar:`GRAYLOG_URL`, :envvar:`MAX_IN_FLIGHT` ``10`` ve :envvar:`MAX_ATTEMPTS` ``5`` ile birlikte aşağıdaki değişkenler kullanılır.


.. envvar:: TEMIP_URL
  
  TeMIP webservis adresi.

  :Örnek: ``http://10.x.x.x:9999/submitalarm/SubmitAlarmService/``


.. envvar:: REQUEUE_DELAY
  
  Webservise erişim konusunda problem olduğunda tekrar denemeden önce beklenecek süre.
  
  :Format: ``süre`` [#duration]_
  :Varsayılan: ``5 seconds``


.. envvar:: TIMEOUT
  
  Webservise yapılan HTTP isteği için zaman aşımı.
  
  :Format: ``süre`` [#duration]_
  :Varsayılan: ``10 seconds``


.. [#env_default] Varsayılan değer olsa da canlı ortamda her zaman için *production* değerinin atanması tavsiye edilir.
.. [#duration] Milisaniye ya da İngilizce ifadeyle süre. `2 dakika` için `2 minutes` ya da `120000`
.. [#custom_default] Lütfen kullanan servisin açıklamasını kontrol edin.
